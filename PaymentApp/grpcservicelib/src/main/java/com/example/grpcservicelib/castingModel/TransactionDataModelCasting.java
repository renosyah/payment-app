package com.example.grpcservicelib.castingModel;

import com.example.modellib.transactionDataModel.TransactionDataModel;
import transaksi.Transaksi;

public class TransactionDataModelCasting {
    public static TransactionDataModel toTransaksiDataModel(Transaksi.transaksiData transaksiData){
        TransactionDataModel model = new TransactionDataModel();
        try {

            model.IdTransaksi = transaksiData.getIdTransaksi();
            model.IdPengirim = transaksiData.getIdPengirim();
            model.IdPenerima = transaksiData.getIdPenerima();
            model.Nominal = transaksiData.getNominal();
            model.WaktuTransaksi = transaksiData.getWaktuTransaksi();

        }catch (NullPointerException e){
            return model;
        }

        return model;
    }

    public static Transaksi.transaksiData toTransaksiDataModelGRPC(TransactionDataModel transaksiData){
        return Transaksi.transaksiData
                .newBuilder()
                .setIdTransaksi(transaksiData.IdTransaksi)
                .setIdPengirim(transaksiData.IdPengirim)
                .setIdPenerima(transaksiData.IdPenerima)
                .setNominal(transaksiData.Nominal)
                .setWaktuTransaksi(transaksiData.WaktuTransaksi)
                .build();
    }
}
