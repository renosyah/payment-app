package com.example.grpcservicelib.transaksi_grpc;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import com.example.grpcservicelib.castingModel.TransactionDataModelCasting;
import com.example.modellib.networkConfig.NetworkConfig;
import com.example.modellib.transactionDataModel.TransactionDataModel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import transaksi.Transaksi;
import transaksi.transaksiServiceGrpc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.modellib.StaticVariable.Authorization;
import static com.example.modellib.StaticVariable.PORT;
import static com.example.modellib.StaticVariable.URL;

public class SendGetAllTransaksi extends AsyncTask<Void,Void,ArrayList<TransactionDataModel>> {

    private static SendGetAllTransaksi _instance;
//    private String ClientKey,ClientToken;
    private NetworkConfig networkConfig;
    private Metadata header = new Metadata();
    private TransactionDataModel transaksiDataModel = new TransactionDataModel();
    private ManagedChannel channel;
    private OnSendGetAllTransaksiListener listener;
    private ArrayList<String> Errors = new ArrayList<>();

    public static SendGetAllTransaksi newBuilder(){
        _instance = new SendGetAllTransaksi();
        return _instance;
    }

//    public SendGetAllTransaksi setKey(String ClientKey, String ClientToken) {
//        _instance.ClientToken = ClientToken;
//        _instance.ClientKey = ClientKey;
//        return _instance;
//    }

    public SendGetAllTransaksi setNetworkConfig(NetworkConfig networkConfig) {
        _instance.networkConfig = networkConfig;
        return _instance;
    }

    public SendGetAllTransaksi setIdPengguna(Long IdPengirim) {
        _instance.transaksiDataModel.IdPenerima = IdPengirim;
        return _instance;
    }

    public SendGetAllTransaksi setOnSendGetAllTransaksiListener(OnSendGetAllTransaksiListener listener) {
        _instance.listener = listener;
        return _instance;
    }

    public void send(){
        if (_instance != null){
            _instance.execute();
        }
    }

    private SendGetAllTransaksi() { }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Metadata.Key<String> key = Metadata.Key.of(Authorization, Metadata.ASCII_STRING_MARSHALLER);
//        _instance.header.put(key, _instance.ClientToken == null ? "" : _instance.ClientToken);
//        _instance.header.put(key, _instance.ClientKey== null ? "" : _instance.ClientKey);
        _instance.channel = ManagedChannelBuilder
                .forAddress(
                        _instance.networkConfig != null ? _instance.networkConfig.getUrl() : URL,
                        _instance.networkConfig != null ? _instance.networkConfig.getPort() : PORT
                )
                .usePlaintext(true)
                .build();
    }

    @Override
    protected ArrayList<TransactionDataModel> doInBackground(Void... voids) {

        ArrayList<TransactionDataModel> response = new ArrayList<>();
        try {

            transaksiServiceGrpc.transaksiServiceBlockingStub stub = transaksiServiceGrpc
                    .newBlockingStub(_instance.channel);

            stub = MetadataUtils.attachHeaders(stub,_instance.header);

            Iterator<Transaksi.transaksiData> transaksiDataIterator = stub.allTransaksi(TransactionDataModelCasting.toTransaksiDataModelGRPC(_instance.transaksiDataModel));

            while (transaksiDataIterator.hasNext()){

                Transaksi.transaksiData transaksiData = transaksiDataIterator.next();

                if (transaksiData.getErrorsMessageList() != null && transaksiData.getErrorsMessageCount() > 0){
                    Errors.addAll(transaksiData.getErrorsMessageList());
                }

                response.add(TransactionDataModelCasting.toTransaksiDataModel(transaksiData));
            }

        }catch (Exception e){
            _instance.Errors.add(e.getMessage());
        }

//        Errors.add(_instance.ClientKey == null && _instance.ClientToken == null ?
//                "All key not set!" : _instance.ClientKey == null ?
//                "Client key not set!" :
//                _instance.ClientToken == null ?
//                        "Token key not set!" :
//                        "");

        return response;
    }



    @Override
    protected void onPostExecute(ArrayList<TransactionDataModel> transaksiDatas) {
        super.onPostExecute(transaksiDatas);

        try {
            _instance.channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

            _instance.Errors.add(e.getMessage());
            if (_instance.listener != null) {

                _instance.listener.onErrorGetAllTransaksi(Errors);
            }
        }

        if (_instance.Errors.size() > 0 && _instance.listener != null){
            _instance.listener.onErrorGetAllTransaksi(_instance.Errors);
        }


        if (_instance.listener != null){
            _instance.listener.onGetAllTransaksi(transaksiDatas);
        }
    }

    public interface OnSendGetAllTransaksiListener {
        void onErrorGetAllTransaksi(@NonNull List<String> errors);
        void onGetAllTransaksi(@NonNull ArrayList<TransactionDataModel> transaksis);
    }
}
