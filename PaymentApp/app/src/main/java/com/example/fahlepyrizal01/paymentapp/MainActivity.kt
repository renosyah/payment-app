package com.example.fahlepyrizal01.paymentapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.grpcservicelib.transaksi_grpc.SendGetAllTransaksi
import com.example.grpcservicelib.transaksi_grpc.SendGetAllTransaksi.OnSendGetAllTransaksiListener
import com.example.modellib.networkConfig.NetworkConfig
import com.example.modellib.transactionDataModel.TransactionDataModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(),
    View.OnClickListener,
    OnSendGetAllTransaksiListener {

    var errorsLog = ""
    val networkConfig = NetworkConfig.newBuilder().setUrl("172.18.0.237").setPort(8000)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initiationWidget()
    }

    private fun initiationWidget(){

        button.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when (v){
            button -> {
                getAllTransaksi()
            }
        }

        errorsLog = ""
    }

    fun showError(errors: MutableList<String>){
        for (error in errors) {
            errorsLog += error + "\n"
        }

        textView2.text = if (errorsLog != "") errorsLog else "No Error, data is Ok"
    }

    override fun onGetAllTransaksi(transactions: ArrayList<TransactionDataModel>) {
        var datas = "Transactions List\n"
        for (data in transactions){
            datas += "${data.IdTransaksi}\n"
            datas += "${data.IdPengirim}\n"
            datas += "${data.IdPenerima}\n"
            datas += "${data.Nominal}\n"
            datas += "${data.WaktuTransaksi}\n"
            datas += "\n"
        }
        textView.text =  datas
    }

    override fun onErrorGetAllTransaksi(errors: MutableList<String>) {
        showError(errors)
    }

    fun getAllTransaksi(){

        SendGetAllTransaksi.newBuilder()
            .setIdPengguna(148591304110702511)
            .setNetworkConfig(networkConfig)
//            .setKey("x9O1LkXjyxpRiyhNRX8T", "auth5cur3")
            .setOnSendGetAllTransaksiListener(this)
            .send()
    }
}
